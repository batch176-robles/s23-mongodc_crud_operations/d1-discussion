// CRUD OPERATIONS


/* 
    Syntax:
        Inserting One Document
        --db.collectionName.insertOne({
                "fieldNameA": "valueA",
                "fieldnameB": "valueB"
        })

*/

db.users.insertOne({
    "firstname": "Jane",
    "lastname": "Doe",
    "age": 33,
    "email": "jane.doe@email.com",
    "Department": "Sales Dept"
})

/* 
    Insert Many Documents

    Syntax:
        -db.collectionName.insertMany([
            {
                "fieldA": "valueA",
                "fieldB": "valueB"
            }
            {
                "fieldA": "valueA",
                "fieldB": "valueB"
            }
        ])
*/

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com"
        "department": "none"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "none"
    },
])

/* 
    Mini Activity:
        1. Make a new collection with the name "courses"
        2. Insert the following fields and values

        name: Javascript 101,
        price: 5000
        description: Introduction to Javascript
        isActive: true

        name: HTML 101,
        price: 2000,
        description: Introduction to HTML
        isActive: true

        name: CSS 101,
        price: 2500
        description: Introduction to CSS
        isActive: false
*/

db.courses.insertMany([
    {
        "name": "Javascript 101",
        "price": 5000,
        "description": "Introduction to Javascript",
        "isActive": true
    },
    {

        "name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML"
        "isActive": true
    },
    {
        "name": "CSS 101",
        "price": 2500,
        "description": "Introduction to CSS"
        "isActive": false
    }
])

// Read - Find Documents

/* 
    Syntax:
        -db.collectionName.find() - this will retrieve all our documents
        -db.collectionName.fin({"criteria": "value"}) - will retrieve all documents that match our criteria
        -db.collectionName.findOne({"criterial": "value"}) - will return the first document in our collection that match our criteria.
        -db.collectionName.findOne({}) - will return the first document in our collection

*/

db.users.find()

db.users.findOne({})

db.users.find({
    "lastName": "Armstrong",
    "age": 82
})

// Update Documents
/* 
    Syntax:
        db.collectionName.updateOne({
            "criteria": "fieldValue"
        },
        {
            $set: {
                "fieldToBeUpdate": "updateValue"
            }
        })

        db.collectionName.updateMany(
            {
                "criteria": "field"
            },
            {
                $set: {
                    "fieldToBeUpdate": "updatevalue"
                }
            })
*/

db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "age": 0,
    "email": "test@mail.com",
    "department": "none"
})

db.users.updateOne({
    "firstName": "Test",
},
    {
        $set: {
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "email": "billgates@mail.com",
            "department": "operations",
            "status": "active"
        }
    })

db.users.updateMany({
    "department": "none",
},
    {
        $set: {
            "department": "HR"
        }
    })

// Removing a field
db.users.updateOne({
    "firstName": "Bill",
},
    {
        $unset: {
            "status": "active"
        }
    })

// Mini Activity
/* 
    1. Update the HTML 101 Course
        - Make the isActive to false

    2. Add enrollees field to all the docouments in our courses collection
        - Enrollees: 10

*/


// db.courses.insertMany([
//     {
//         "name": "Javascript 101",
//         "price": 5000,
//         "description": "Introduction to Javascript",
//         "isActive": true
//     },
//     {

//         "name": "HTML 101",
//         "price": 2000,
//         "description": "Introduction to HTML"
//         "isActive": true
//     },
//     {
//         "name": "CSS 101",
//         "price": 2500,
//         "description": "Introduction to CSS"
//         "isActive": false
//     }
// ])

db.courses.updateOne({
    "firstName": "Bill",
},
    {
        $unset: {
            "status": "active"
        }
    })